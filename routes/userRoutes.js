const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const userService = require('../services/userService');
// const { delete } = require('./authRoutes');

const router = Router();

// TODO: Implement route controllers for user
//router.use(UserService());

// GET: /user
// отримання масиву всіх користувачів
// GET: /user/:id
// отримання одного користувача по ID
// POST: /user
// створення користувача за даними з тіла запиту
// PUT: /user/:id
// оновлення користувача за даними з тіла запиту
// DELETE: /user/:id
// видалення одного користувача по ID


router.get('/', function(req, res) {
    console.log('get user');
    res.send('user service get');
    userService();
    responseMiddleware(req, res);
  });
router.get('/:id', function(req, res){
    console.log('get usr by id', req.params.uid);
    userService();
});
router.post('/', function(req, res){
    console.log('user create');
    createUserValid(req);
    responseMiddleware(req, res);
});
router.put('user/:id', function(req, res){
    updateUserValid(req);
    responseMiddleware(req, res);
});
router.delete('user/:id', function(req, res){
    console.log('delete user b id')
});


module.exports = router;